
package matrices;
import com.sun.xml.internal.fastinfoset.util.CharArray;
import javax.swing.JOptionPane;
import java.util.Scanner;
import java.lang.Math;

public class Matrices {

    public static void main(String[] args) {
        //matriz1();
        //matrizCien();
        //matrizFrase();
        //matrizNotas();
        //matizCapicua();
        //aleatoriosNumeros();
        //cuentaLetras();
        //cambiaNumeros();
        //camNum2();
        //temperaturas();
        //temperaturas2();
        //notasBidi();
        //aleatorios();
        bidiCuadrado();
    }
    public static void matriz1(){
        float[] Notas= 
           {5.8f,6.2f,7.1f,5.9f,3.6f,9.9f,1.2f,10.0f,4.6f,5.0f}; // f es para difernciar un double de un float
        
        String [] Nombres = new String[10];
        Nombres[0]="Pedro";Nombres[1]="Ana";Nombres[2]="Luis";
        Nombres[3]="Luis";Nombres[4]="Juan";Nombres[5]="Eva";
        Nombres[6]="Mari";Nombres[7]="Fran";Nombres[8]="Luz";
        Nombres[9]="Sol";
        
        for (int i=0;i<Nombres.length;i++)
            System.out.println(Nombres[i] + " : " +Notas[i]);
        
        System.out.println(); // Salto de linea
        
        byte Aprobados = 0;
        String NombresAprobados = new String();
        for (int i=0;i<Nombres.length;i++)
            if (Notas[i]>=5.0){
                Aprobados++;
                NombresAprobados = NombresAprobados+" "+Nombres[i];
            }
        System.out.println("Aprobados: " + Aprobados);
        System.out.println("Aprobados:" + NombresAprobados);
    }
    
    /*Crea un array de números de 100 posiciones, 
        que contendrá los números del 1 al 100. 
        Obtén la suma de todos ellos y la media.*/

    public static void matrizCien(){
        int sumaArray=0;
        int [] num = new int[100];
        
        for(int i=0;i<num.length;i++){
            num[i]= i+1; 
            sumaArray= sumaArray + num[i];
        }
        double media= sumaArray / num.length;
        System.out.println("La suma es: " +sumaArray);
        System.out.println("La media es: " + media); 
    }
    
    /*Pide al usuario por teclado una frase y 
        pasa sus caracteres a un array de caracteres.
        Pista. Necesitareis un método que vimos ayer*/
    
    public static void matrizFrase(){
        String frase = JOptionPane.showInputDialog("Pon una frase sin nada raro cacho carne");
        char [] caracter = new char[frase.length()];
        //Primero almaceno todas las letras de la palabra en el array caracter
        for(int i=0;i<frase.length();i++){
            caracter[i]=frase.charAt(i);
            
        }
        //Ahora recorro el array en orden inverso para imprimir todas las letras
        for(int j=frase.length()-1;j>=0;j--){
            
            System.out.print(caracter[j]);
        }
        System.out.println();
    }
    
    /*Crea un array de números y otro de Strings de 10 posiciones donde insertaremos notas entre 0 y 10 
    pudiendo ser decimal la nota en el array de números, en el de Strings se insertaran los nombres de los alumnos. 
    Una vez insertada impriremos la nota de los alumnos de la siguiente forma: 
    "El alumno John Doe ha sacado un 8.5"*/
    // Ampliacion
    /*El número de alumnos de los que pedir la nota se deberá introducir por teclado antes de comenzar a pedir datos*/

    
    public static void matrizNotas(){
        /*double[] Notas = new double[3];
        String [] Nombres = new String[3];
        String paso, chicos;
        double nota;
            for (int j = 0; j< Nombres.length; j++){
            chicos = JOptionPane.showInputDialog("Introduce el nombre del giliiii... que sea");
            
            for (int i = 0;i < Notas.length; i++){
            paso = JOptionPane.showInputDialog("Introduce la nota espero que apruebes");
            nota = Double.parseDouble(paso);
            
            //System.out.println("El alumno/pringado "+chicos + " ha sacado un: " +nota); break;
            if(nota<=5) System.out.println("El alumno/pringado "+chicos + " ha sacado un: " +nota+ " necesitas estudiar mas GILIIIII..."); 
            else   System.out.println("El alumno "+chicos + " ha sacado un: " +nota);
                if (nota>=9) System.out.println(chicos+ " como se nota que eres el favorito.");break;
        }
        }*/
        
        String n_alumnos = JOptionPane.showInputDialog("Introduce el numero de alumnos");
        int numAlum = Integer.parseInt(n_alumnos);
        
        float [] notas = new float[numAlum];
        String [] nombres = new String [numAlum];       
        int indice;
        String leenota;
        for (int i=0;i<nombres.length;i++){
            indice=i+1;
            nombres[i]=JOptionPane.showInputDialog("Introduce nombre número "+indice);
            leenota=JOptionPane.showInputDialog("Introduce nota del alumno "+nombres[i]);
            notas[i]=Float.parseFloat(leenota);
        }
        
        for (int j=0;j<nombres.length;j++) {
            System.out.println("El alumno "+nombres[j]+" ha sacado un "+notas[j]);};
    
    }
    
    /*Crea una aplicación que pida un numero por teclado y después comprobaremos si el numero introducido es capicua,
    es decir, que se lee igual sin importar la dirección.
    Por ejemplo, si introducimos 30303 es capicua, si introducimos 30430 no es capicua.*/
    
    public static void matizCapicua(){
        boolean capicua = true;
        int fin;
        String num = JOptionPane.showInputDialog("Introduce el numero para ver si es capicua ");
       /* int numero[] = new int [num.length()];
        
        for(int i=0;i<numero.length;i++){
            numero[i]=Character.getNumericValue(num.charAt(i));        
        }
        
        fin = numero.length-1;
        for (int j=0;j<(numero.length/2);j++){
            if(numero[j]!=numero[fin]) capicua=false;
            fin--;
        }*/
       
       fin = num.length()-1;
       for (int j=0;j<(num.length()/2);j++){
            if(num.charAt(j)!=num.charAt(fin)) capicua=false;
            fin--;
        }
        if (capicua) System.out.println("El numero es capicua");
        else System.out.println("El numero NO es capicua");
        
    }
    
    /*Crea un array de números de un tamaño pasado por teclado, 
    el array contendrá números aleatorios entre 1 y 300*/
    
    public static void aleatoriosNumeros(){
       int tamaño, num, contador=0;       
       
       Scanner entrada = new Scanner(System.in);
       System.out.print("Pon el tamaño del array: ");
       tamaño = entrada.nextInt();
       
        Scanner entrada1 = new Scanner(System.in);
        System.out.print("Acaba en: ");
        num = entrada1.nextInt();
       
       int numeros[]= new int [tamaño];
       for (int i = 0; i < numeros.length; i++) {
            numeros[i]=(int) (Math.random() * 300+1);
            System.out.println(numeros[i]);

            if(numeros[i]%10==num) contador++;
            
       }
       System.out.println();
       System.out.println("Los numero que acaban en " +num+ " son:" +contador);
    }
    
    /*Escribe un programa java que pida al usuario que introduzca un texto y una letra. 
    Después el programa tiene que calcular y presentar por pantalla, 
    cuantas veces aparece la letra en el texto.*/
    
    public static void cuentaLetras(){
        int contador=0;
        //String letra="";
        String frase = JOptionPane.showInputDialog("Introduce una frase");
        //Scanner entrada = new Scanner(System.in);
        char caracter= dameLetra();
              
       /* while (letra.length()!=1){
            System.out.print("Introduce la letra a contar: ");
            letra=entrada.nextLine();
        }
        
        char caracter= letra.charAt(0);*/
        
        for (int i = 0; i < frase.length(); i++) {
            if(frase.charAt(i)==caracter) contador++;
        }
        
        if (contador==1) System.out.println("En la frase " +frase+ " aparece la letra " +caracter+ " " +contador+ " vez");
        else System.out.println("En la frase " +frase+ " aparece la letra " +caracter+ " " +contador+ " veces");
    }
    
    public static char dameLetra(){
        String letra="";      
        while (letra.length()!=1){
            letra = JOptionPane.showInputDialog("Introduce una UNICA letra");
        }
        return letra.charAt(0);
    }
    
    /*Escribe un programa que busque un valor dentro de un array de integers (defínelo tú internamente,
    sin pedírselo al usuario) y lo sustituya por un cero.
    El valor a buscar y borrar debe ser introducido por el usuario.
    El programa debe mostrar por pantalla el valor borrado, el array inicial y el array final.*/
    
    public static void cambiaNumeros(){
        int num,contador=0;
        Scanner entrada = new Scanner(System.in);
        System.out.print("Indica el numero para quitarlo y preparate para cagarla fijo ");
        num = entrada.nextInt();
        
        int numeros[] = {1,2,3,4,5,6,7,8,9,1,4,7,8,5,2,3,6,9};
        int numAux[] =  {1,2,3,4,5,6,7,8,9,1,4,7,8,5,2,3,6,9}; 
               
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i]==num){
                numeros[i]=0;
                contador++;
            }           
        }
        
        System.out.println("El array final es: ");
        for (int j = 0; j < numeros.length; j++) {
            System.out.print(numeros[j]+" ");
        }
        
        System.out.println();
        System.out.println("Se ha encontrado "+contador+" ocurrencias del numero "+num);
        
        System.out.println("El array original es: ");
        for (int k = 0; k < numAux.length; k++) {
            System.out.print(numAux[k]+" ");
        }   
    }

    /*Rehaced el ejercicio anterior pero 
    ahora pediremos además al usuario el número de elementos del array 
    y lo generaremos automáticamente con números entre 1 y 9*/
    
    public static void camNum2(){
        int num, tamaño, contador=0;
        
        String medida = JOptionPane.showInputDialog("Indica el tamaño del array ");
        tamaño = Integer.parseInt(medida);
        //pedimos un numero al usuario para sustituirlo por cero en el array
        Scanner entrada = new Scanner(System.in);
        System.out.print("Indica el numero para quitarlo y preparate para cagarla fijo ");
        num = entrada.nextInt();
        
        int [] numeros  = new int [tamaño];
        int [] original = new int [tamaño];
        
        //lo primero es generar aleatorios entre 1 y 9 para rellenart el array
        for (int i = 0; i < original.length; i++) {
            original[i]= (int) (Math.random()*9+1);
            
            if(original[i]==num){ numeros[i]=0;
                contador++;
            }else numeros[i]=original[i];

        }
        
        System.out.println("El array final es: ");
        for (int j = 0; j < numeros.length; j++) {
            System.out.print(numeros[j]+" ");
        }
        
        System.out.println();
        System.out.println("Se ha encontrado "+contador+" ocurrencias del numero "+num);
        
        
        System.out.println("El array original es: ");
        for (int l = 0; l < original.length; l++) {
            System.out.print(original[l]+" ");
        }
    }
    
    /*Crea una función que rellene una matriz de 12x30 con las temperaturas 
        de Alpelandia. Se rellenará con doubles con valores entre 0 y 35.
        Una vez hecho esto se pedirá un número de mes al usuario y mostrará
        las temperaturas de ese mes*/
    
    public static void temperaturas() {
        int mes;
        
        double [][] temperaturas = new double [12][30];
        
        
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 30; j++) {
                temperaturas[i][j]= (Math.random() * 35+0);
            }
        }
        
        Scanner entrada = new Scanner(System.in);
        System.out.print("Indica el mes ");
        mes = entrada.nextInt();
        
        for (int k = 0; k < 30; k++) {
            System.out.printf("Las tempe del dia "+(k+1)+" del mes " +mes+ " son : %.2f %n",temperaturas[mes-1][k]);
        }   
    }
    
    public static void temperaturas2() {
        int mes;
        double max=0, min=35, suma=0;
        double [][] temperaturas = new double [12][30];
        
        
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 30; j++) {
                temperaturas[i][j]= (Math.random() * 35+0);
            }
        }
        
        Scanner entrada = new Scanner(System.in);
        System.out.print("Indica el mes ");
        mes = entrada.nextInt();
        System.out.println("Las temperaturas del mes "+mes+" son: ");
        for (int k = 0; k < 30; k++) {
            System.out.printf("Las tempe del dia "+(k+1)+" del mes " +mes+ " son : %.2f %n",temperaturas[mes-1][k]);
            if (temperaturas[mes][k]>max) max = temperaturas[mes][k];
            if (temperaturas[mes][k]<min) min = temperaturas[mes][k];
            suma+= temperaturas[mes][k];
        }   
        System.out.printf("La maxima y minima del mes "+mes+" son: %.2f %n y %.2f %n",max,min);
        System.out.printf("la temperatura media es %.2f %n ",suma/30);
    }
    
    //Crea un array de 10 filas y dos columnas
    //Los nombres de los alumnos se le pedirán al usuario y las notas se generarán aleatoriamente entre 0 y 10
    //Una vez rellenada la matriz se mostrarán los datos, un alumno en cada línea.
    
    public static void notasBidi(){
        double nota;
        String [][] alumNotas = new String[10][2];
               
        for (int i = 0; i < 10; i++) {
            alumNotas[i][0] = JOptionPane.showInputDialog("Introduce el nombre del lelo que quieras: ");  
            nota=(Math.random() * 10+0);
            nota= Math.round(nota*100.0)/100.0;
            alumNotas[i][1]=Double.toString(nota);
        }
        System.out.println("Susu  Nota");
        for (int j = 0; j < 10; j++) {
            System.out.print(alumNotas[j][0]+ "  ");
            System.out.println(alumNotas[j][1]);
        }
    }
    
    /*Pide un número por teclado y genera una matriz de números aleatorios 
    de ese tamaño. Al final, muestralos todos por pantalla*/
    
    public static void aleatorios(){
        int tama;
        
        Scanner entrada = new Scanner(System.in);
        System.out.print("Introduce el tamaño del array ");
        tama = entrada.nextInt();
        
        double [] numeros = new double[tama];
        
        for (int i = 0; i < numeros.length; i++) {
            numeros[i]= (Math.random()*100+1);
            System.out.println(Math.round(numeros[i]*100.0)/100.0);
        }
    }
    
    /*Crea un array de 10 enteros que iremos pidiendo al usuario por teclado.
    En un array bidimensional almacenaremos en la primera columna el número y en la segunda su cuadrado.*/

    public static void bidiCuadrado(){
        int pide, indice=0;
        Scanner entrada = new Scanner(System.in);
        int [][] numeros = new int [10][2];
        
        for (int i = 0; i < 10; i++) {
        indice=i+1;    
        System.out.print("Introduce un numero "+indice+" ");
        pide = entrada.nextInt();
        numeros[i][0]= pide;
        
        }
        
        System.out.println("Numero  Cuadrado");
        for (int j = 0; j < 10; j++) {
            System.out.print(numeros[j][0]+"       ");
            numeros[j][1]= numeros[j][0]*numeros[j][0];
            System.out.println(numeros[j][1]);
        }
    }
}       






