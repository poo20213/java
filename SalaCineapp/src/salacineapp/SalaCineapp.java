
package salacineapp;

/**
 *
 * @author miguel
 */
public class SalaCineapp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SalaCine sala1, sala2, sala3, sala4, sala5, sala6;
        
        // Constructor por defecto
        sala1 = new SalaCine();
        sala2 = new SalaCine();
        
        // Constructor con tres datos
        sala3 = new SalaCine(150, "Los Miserables", 7);
        sala4 = new SalaCine(200, "En busca de la felicidad", 5.40);
        
        
        // Constructor con cuatro datos
        sala5 = new SalaCine(90, 40, "Indiana Jones", 6.50);
        sala6 = new SalaCine(125, 60, "Django", 7.14);
        
        
        //Completa los datos que les faltan a las cuatro primeras con los setters 
        sala1.setAforo(60);
        sala1.setPelicula("El guardaespalda");
        sala1.setOcupados(15);
        sala1.setEntrada(5.50);
        
        sala2.setAforo(120);
        sala2.setPelicula("Van Helsing");
        sala2.setOcupados(115);
        sala2.setEntrada(6.30);
        
        sala3.setOcupados(50);
        sala4.setOcupados(25);
        
        // Vaciar sala5
        sala5.vaciar();
        
        //En la sala sala2 y sala3 han entrado tres personas. Haz que esto quede reflejado donde proceda
        sala2.entraUno();
        sala2.entraUno();
        sala2.entraUno();
        
        // Con una linea sala3
        sala3.setOcupados(sala3.getOcupados()+3);
        
        // Utilizando el  metodo entranVarios
        //sala1.entranVarios(25);
        
        
        // Mostrar los datos con el metodo toSring()
        System.out.println(sala1.toString());
        System.out.println(sala2.toString());
        System.out.println(sala3.toString());
        System.out.println(sala4.toString());
        System.out.println(sala5.toString());
        System.out.println(sala6.toString());
        
    }
    
}
