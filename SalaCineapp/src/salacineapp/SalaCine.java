
package salacineapp;

/**
 *
 * @author miguel
 */
public class SalaCine {
    private int aforo;
    private int ocupados;
    private String pelicula;
    private double entrada;

    
    //Constructores
    public SalaCine() {
        this.aforo = 100;
        this.ocupados = 0;
        this.pelicula = "";
        this.entrada = 5;
    }

    public SalaCine(int aforo, int ocupados, String pelicula, double entrada) {
        this.aforo = aforo;
        this.ocupados = ocupados;
        this.pelicula = pelicula;
        this.entrada = entrada;
    }


    public SalaCine(int aforo, String pelicula, double entrada) {
        this.aforo = aforo;
        this.pelicula = pelicula;
        this.entrada = entrada;
        this.ocupados = 0;
    }

    
    // Setters
    public void setAforo(int aforo) {
        this.aforo = aforo;
    }

    public void setOcupados(int ocupados) {
        this.ocupados = ocupados;
    }
    
    public void setLibres(int ocupados){
        this.ocupados = ocupados;
    }

    public void setPelicula(String pelicula) {
        this.pelicula = pelicula;
    }

    public void setEntrada(double entrada) {
        this.entrada = entrada;
    }

    
    //Getters
    public int getAforo() {
        return aforo;
    }

    public int getOcupados() {
        return ocupados;
    }
    
    public int getLibres(){
        return this.aforo-this.ocupados;
    }
    
    public double getPorcentaje(){
        return (this.aforo/this.ocupados)*100;
    }
    
    public int getIngresos(){
        return (int) (this.ocupados*this.entrada);
    }

    public String getPelicula() {
        return pelicula;
    }

    public double getEntrada() {
        return entrada;
    }
    
    //Metodos
    
    public void vaciar(){
        this.ocupados=0;
        this.pelicula="";
    }
    
    public void entraUno(){
        this.ocupados++;
    }

    @Override
    public String toString() {
        return "Para la pelicula " + this.pelicula + " la sala de cine con un aforo de " + this.aforo + " personas, tiene " + this.ocupados + " butacas ocupadas debido a que la entrada costaba " +this.entrada+ " mira que era mala la puata peli.";
    }
    
    public void entranVarios(int numero){
        this.ocupados+=numero;
    }
}
