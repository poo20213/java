/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

/**
 *
 * @author miguel
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Cuenta Española, Suiza;
        Española = new Cuenta("Pepe");
        Suiza = new Cuenta("J.C.1º", 50000);
            //Ingresa dinero de las cuentas
        Española.setIngresar(1000);
        Suiza.setIngresar(400);
            //Retiramos el dinero de la cuenta
        Española.setRetirar(500);
        Suiza.setRetirar(100);
            //Informacion de la cuenta
        Española.Info();
        Suiza.Info();
    }    
}
