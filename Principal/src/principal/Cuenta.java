/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

/**
 *
 * @author miguel
 */
public class Cuenta {
    //Atributos
    String titular;
    double cantidad;
    //Constructores
    public Cuenta (String titular, double cantidad)  {
        this.titular = titular;
        this.cantidad = cantidad;  
    }
    public Cuenta (String titular)  {
        this.titular = titular;
        this.cantidad = 0;
    }  
    //Metodos
    public void setTitular(String titular){ //establece dato
        this.titular = titular;
    }

    public String getTitular(){ //Devuelve el dato
        return titular;
    }
    void setCantidad(double cantidad){
        this.cantidad = cantidad;
    }

    public double getCantidad(){
        return cantidad;
    }

    public void setIngresar(double cantidad){
        if (cantidad > 0){
            this.cantidad += cantidad;
        // La forma tradicional seria
        //this.cantidad = this.cantidad + cantidad
        }
    }
    public void setRetirar(double cantidad){
        if (this.cantidad - cantidad < 0){
            this.cantidad = 0;
        }else{
            this.cantidad -= cantidad;
        }
    }
    public void Info(){
        System.out.println("El titular es: " +
            titular + " Con:" + this.cantidad + " EUR en su cuenta.");
}
}
