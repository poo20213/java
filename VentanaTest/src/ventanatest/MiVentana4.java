
package ventanatest;

import java.awt.*;
import javax.swing.*;

public class MiVentana4 extends JFrame{
    public MiVentana4(){
        super("Demo application");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(300, 200);
        
        JPanel datos = new JPanel();
        GridLayout gl = new GridLayout(3, 2);
        datos.setLayout(gl);
        datos.add(new JLabel("User: "));
        datos.add(new JTextField(10));        
        datos.add(new JLabel("Password: "));
        datos.add(new JTextField(10));
        
        JPanel botones = new JPanel();
        botones.setLayout(new FlowLayout());
        botones.add(new JButton("login"));
        botones.add(new JButton("register"));
        
        Container cp = getContentPane();
        cp.add(datos, BorderLayout.CENTER);
        cp.add(botones, BorderLayout.SOUTH);
    }
}
