/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanatest;

import java.awt.*;
import javax.swing.*;

/**
 *
 * @author miguel
 */
public class MiVentana extends JFrame{

    public MiVentana() {
        // La clausula super invoca al constructor de la clase padre equivalente
        super ("Titulo de ventana");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container cp = getContentPane();
        cp.setLayout(new FlowLayout());
        JLabel etiqueta = new JLabel("Nombre: ");
        JTextField texto = new JTextField(20);
        JButton boton = new JButton("Saludar");
        boton.setBackground(Color.red);
        cp.add(etiqueta);
        cp.add(texto);
        cp.add(boton);
    }
    
    public MiVentana(int ancho, int alto){
        super ("Titulo de ventana");
        this.setSize(ancho, alto);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public MiVentana(int ancho, int alto, String titulo){
        super(titulo);
        this.setSize(ancho, alto);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
}
