
package ventanatest;

import java.awt.*;
import javax.swing.*;

public class BotonVentana extends JFrame{
    public BotonVentana(){
        super("Botón");
        this.setSize(300, 200);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container cp = getContentPane();
        cp.setLayout(new FlowLayout());
        JButton boton = new JButton("¡Púlsame!");
        boton.addActionListener(new EventoBotonPulsado());
        cp.add(boton);
    }
}
