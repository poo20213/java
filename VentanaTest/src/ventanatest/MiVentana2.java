
package ventanatest;

import java.awt.*;
import javax.swing.*;


public class MiVentana2 extends JFrame{
    public MiVentana2(){
        super ("El p* telefono");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container cp = getContentPane();
        GridLayout gl = new GridLayout(4, 3);
        gl.setHgap(5); gl.setVgap(5);
        cp.setLayout(gl);
        for (int i = 1; i <= 9; i++) {
            cp.add(new JButton(String.valueOf(i)));
        }
        cp.add(new JButton("*"));
        cp.add(new JButton("0"));
        cp.add(new JButton("#"));
    
        //Constructor de MiVentana2 que reciba por parámetro ancho, alto 
        //y los huecos horizontales y verticales entre botones
    }
        public MiVentana2(int ancho, int alto, int huecoh, int huecov){
        super ("El p* telefono");
        this.setSize(ancho, alto);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container cp = getContentPane();
        GridLayout gl = new GridLayout(4, 3);
        gl.setHgap(huecoh); gl.setVgap(huecov);
        cp.setLayout(gl);
        for (int i = 1; i <= 9; i++) {
            cp.add(new JButton(String.valueOf(i)));
        }
        cp.add(new JButton("*"));
        cp.add(new JButton("0"));
        cp.add(new JButton("#"));
    }
        
        public MiVentana2(int huecoh, int huecov){
        super ("El p* telefono");
        this.setSize(800, 750);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container cp = getContentPane();
        GridLayout gl = new GridLayout(4, 4);
        gl.setHgap(huecoh); gl.setVgap(huecov);
        cp.setLayout(gl);
        for (int i = 1; i <= 13; i++) {
            cp.add(new JButton(String.valueOf(i)));
        }
        cp.add(new JButton("*"));
        cp.add(new JButton("0"));
        cp.add(new JButton("#"));
    }
}