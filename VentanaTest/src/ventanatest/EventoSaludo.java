/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanatest;

import java.awt.event.*;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


public class EventoSaludo implements ActionListener{
    private JTextField cuadroTexto;
    
    public EventoSaludo(JTextField cuadroTexto){
        this.cuadroTexto = cuadroTexto;
    }
    
    public void actionPerformed(ActionEvent e){
        JOptionPane.showMessageDialog(null, "Hola, " +
                cuadroTexto.getText() + "!");
    }
}

