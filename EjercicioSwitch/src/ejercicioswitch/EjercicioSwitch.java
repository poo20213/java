package ejercicioswitch;
import javax.swing.JOptionPane;
/**
 *
 * @author miguel
 */
public class EjercicioSwitch {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String diaString, minusc;
        
        String dia = JOptionPane.showInputDialog("Introduce día de la semana");
        minusc=dia.toLowerCase();
        
        switch(minusc){
            case "lunes": diaString = "Laboral";
                    break;
            case "martes": diaString = "Laboral";
                    break;
            case "miercoles": diaString = "Laboral";
                    break;
            case "miércoles": diaString = "Laboral";
                    break;        
            case "jueves": diaString = "Laboral";
                    break;
            case "viernes": diaString = "Laboral";
                    break;
            case "sabado": diaString = "No laboral";
                    break;
            case "sábado": diaString = "No laboral";
                    break;        
            case "domingo": diaString = "No laboral";
                    break;
            default: diaString = "Dia inválido";
                     break;        
        }
      System.out.println("Es " + dia + " y es " + diaString);      
    }
    
}
