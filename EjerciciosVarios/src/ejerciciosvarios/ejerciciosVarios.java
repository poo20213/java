
package ejerciciosVarios;
import com.sun.org.apache.bcel.internal.generic.SWITCH;
import javax.swing.JOptionPane;
import java.lang.Math;
import jdk.nashorn.internal.parser.TokenType;
import java.util.Scanner;
import java.text.DecimalFormat;

public class ejerciciosVarios {

     public static void main(String[] args) {
     //Descomenta la función que quieras utilizar y dale al play
     
     //tipoDia();
     //operando();
     //numeroDiasMes();
     //geometria();
     //tiempo();
     //tiempoWhile();
     //contraseña();
     //aleatorio();
     //aleatorioFer();
     //miNumero();
     //factorial();
     //factorialv1();
     //cifras();
     //matematicas();
     //menuMatematico();
     //mayorEdad();
     //mayorEdadV1();
     //promedio();
     //matriculados();
     //cuentaNumeros();
     //decremento();
     //banco();
     //muchoNumero();
     //numPrimo();
     cuentaVocales();
     }    
     
     
     public static void tipoDia(){
     String tipodia,minusc;
     String dia=JOptionPane.showInputDialog("Introduce un dia de la semana");
     
     minusc=dia.toLowerCase();
     
     switch(minusc){
            case "lunes": 
            case "martes":
            case "miercoles":
            case "miércoles":
            case "jueves":
            case "viernes":  tipodia=" día laborable";   break;
            case "sabado":
            case "sábado":
            case "domingo": tipodia=" día festivo"; break;
            default: tipodia=" día erroneo";
        }  
     System.out.println("El día introducido es "+dia+" y es un "+tipodia);
     }
     
     
     public static void operando(){   
        double x, y, resultado;
        String op1=JOptionPane.showInputDialog("Introduce un número");
        x=Double.parseDouble(op1);
        String op2=JOptionPane.showInputDialog("Introduce otro número");
        y=Double.parseDouble(op2);
        String operador=JOptionPane.showInputDialog("Introduce el operador a utilizar");
        switch(operador)
        {
              case "-":
                             resultado = x-y;
                             System.out.println("La resta es: " + resultado);
                             break;
              case "+":
                             resultado = x+y;
                             System.out.println("La suma es: " + resultado);
                             break;
              case "*":
                             resultado = x*y;
                             System.out.println("La multiplicación es: " + resultado);
                             break;
              case "/":
                             if(y!=0)
                             {
                                    resultado = x/y;
                                   System.out.println("La división es: " + resultado);
                             }
                             else
                                   System.out.println("no se puede dividir entre cero");
                             break;
              default:
                            System.out.println("Algún dato introducido es erróneo");
        }
               
     }   
        public static void numeroDiasMes() {
        int mes;
        double anno;
        String numero=JOptionPane.showInputDialog("Introduce un número de mes");
        mes=Integer.parseInt(numero);
        String numero2=JOptionPane.showInputDialog("Introduce un número de año");
        anno=Double.parseDouble(numero2);
        switch(mes)
            {
                  case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                               System.out.println("El mes tiene 31 dias");
                               break;
                  case 4: case 6: case 9: case 11:
                               System.out.println("El mes tiene 30 dias");
                               break;
                  case 2:
                               if((anno%4==0 && anno%100!=0) || anno%400==0)
                                       System.out.println("El mes tiene 29 dias");
                               else
                                       System.out.println("El mes tiene 28 dias");
                               break;
                  default:
                                       System.out.println("numero de mes equivocado");
            }
   }
     
    public static void geometria(){
    double base, altura, lado, operacion, radio;
    String baseS, alturaS, ladoS, radioS;
    String opcion=JOptionPane.showInputDialog("Introduce un área a calcular:\na: triángulo \nb: cuadrado \nc: circulo \nd: rectángulo");
    switch(opcion)
    {
          case "a": case "A":
                   baseS =JOptionPane.showInputDialog("Introduce la base del triángulo");
                   base=Double.parseDouble(baseS);
                   alturaS =JOptionPane.showInputDialog("Introduce la altura del triángulo");
                   altura=Double.parseDouble(alturaS); 
                   operacion = (base*altura)/2;
                   System.out.println("El area del triangulo es: "+ operacion);
                   break;
          case "b": case "B":
                   ladoS =JOptionPane.showInputDialog("Introduce el lado del cuadrado");
                   lado=Double.parseDouble(ladoS);
                   operacion = Math.pow(lado,2);
                   System.out.println("El area del cuadrado es: "+ operacion);
                   break;
          case "c": case "C":
                   radioS =JOptionPane.showInputDialog("Introduce el radio del círculo");
                   radio=Double.parseDouble(radioS);
                   operacion = Math.PI*Math.pow(radio,2);
                   System.out.println("El area del circulo es: "+ operacion);
                   break;
          case "d": case "D":
                   baseS =JOptionPane.showInputDialog("Introduce la base del rectángulo");
                   base=Double.parseDouble(baseS);
                   alturaS =JOptionPane.showInputDialog("Introduce la altura del rectángulo");
                   altura=Double.parseDouble(alturaS); 
                   operacion = (base*altura);
                   System.out.println("El area del rectángulo es: "+ operacion);
                   break;
          default:
                   System.out.println("Opcion incorrecta");
    }
}
    
    public static void tiempo(){
        String day=JOptionPane.showInputDialog("Introduce el dia"); 
        int dia= Integer.parseInt(day);
        if(dia>0 && dia<=31){
            System.out.println("Bien por ti");
        }else{
            System.out.println("No exite la epoca");
        }
        
        String mes=JOptionPane.showInputDialog("Introduce el mes");
        int meses= Integer.parseInt(mes);
        if(meses>0 && meses<=12){
            System.out.println("Bien por ti");
        }else{
            System.out.println("No exite la epoca");
        }
        
        String año=JOptionPane.showInputDialog("Introduce el año");
        int anno= Integer.parseInt(año);
        if(anno>0 && anno<=2050){
            System.out.println("Bien por ti");
        }else if(anno>=0){
            System.out.println("No exite la epoca");
        }else{
            System.out.println("Antes de Cristo");
        }
        
        System.out.println(dia+"/"+meses+"/"+anno);
        
    }
    
   public static void tiempoWhile(){
       String day, mes, año;
       int dia, meses, anno;
       do{
            day= JOptionPane.showInputDialog("Introduce el dia"); 
            dia= Integer.parseInt(day);
       }
       while(dia>31 || dia<1);
       do{
            mes=JOptionPane.showInputDialog("Introduce el mes");
            meses= Integer.parseInt(mes);
       }
       while(meses>12 || meses<1);
       do{
           año=JOptionPane.showInputDialog("Introduce el año");
            anno= Integer.parseInt(año);
       }
       while(anno>2050 || anno<0);
         
        System.out.println(dia+"/"+meses+"/"+anno);   
    }
   
   public static void contraseña(){      
       String contraseña = "ejemplo";
       String contra = "";
       int intentos = 0;
       while(!contraseña.equals(contra) && intentos<3){
            contra =JOptionPane.showInputDialog("Introduce la contraseña");
            intentos++;
       }
       if(contraseña.equals(contra)){
           System.out.println("Mu bien");
       }  else{
           System.out.println("Giliiiiiiiiiiii");
       } 
   }
   
   public static void aleatorio(){
       int numero = (int) (Math.random()*10+1);
       int num;
       String nume;
       int intentos=0;
       
       do{
           nume = JOptionPane.showInputDialog("numero del 1 al 10");
           num = Integer.parseInt(nume);
           if (numero != num){
               System.out.println("Fallaste pringado JAJA");
           };
        intentos++;   
       }while(numero != num);
       
       
       System.out.println("Eureka el numero correcto es: " + numero);
       System.out.println("Lo intentaste: " + intentos+ " veces que matao.");
   }
   
   public static void aleatorioFer(){
    
    int numMagic= (int)(Math.random()*10+1); //Inicializamos la variable, donde vamos a almacenar el numero generado automaticamente
    int numUser;//Inicializamos la variable donde almacenaremos el numero introducido por el usuario
    String dato; //Castearemos numUser y lo convertiremos en una string antes de entrar en el bucle 
    int veces=0;//Inicializamos la variable veces donde vamos a alacenar el numero  de intentos
    
    do{//Entramos en el bucle
        dato= JOptionPane.showInputDialog("Introduce un numero del 1 al 10, compitrueno"); // Formularemos la pregunta con el string DATO
        numUser = Integer.parseInt(dato);//Volveremos a castear para convertir el dato en INT (entero)
       
        veces++;//DENTRO DEL BUCLE DO WHILE, inicizalizamos el contador donde vamos a contar las veces que tiramos
    
    }while(numMagic != numUser); //Le diremos que salga del bucle cuando sean iguales
    
    System.out.println("este es el numero " +numMagic ); //Mostraremos el mensaje cuando
   }
   
   public static void miNumero(){
    int numMagic;
    String dato; 
    int veces=0;
    
    do{
        numMagic= (int)(Math.random()*10+1);
        dato= JOptionPane.showInputDialog("¿el numero " + numMagic+ " es este?");    
        veces++;
    }while(!dato.equals("s") && !dato.equals("S")); 
    
    System.out.println("este es el numero " +numMagic ); 
    System.out.println("lo intentaste " +veces+ " veces." ); 
   }
   
   public static void factorial(){
        double num;
        String dato= JOptionPane.showInputDialog("Mete un numero");
        num = Double.parseDouble(dato);
     
     double numero = num;
     double c;

     
     
     for (c = num -1; c > 1; c--){
          numero *= c;
     }
     System.out.println("El factorial de " +num+ " es: " +numero); 
   }
   
   public static void factorialv1(){
        String dato= JOptionPane.showInputDialog("Introduce un numero para obtener su factorial"); 
        double factorial=1, numero = Double.parseDouble(dato);
        //Solución con while
        while ( numero!=0) 
        {
          factorial=factorial*numero;
          numero--;
        }
        //Solución con for
        for (int x=2;x<=numero;x++)
            factorial = factorial * x;
       
        System.out.println(factorial);
        }
   
   public static void cifras(){
       int cifras; 
       int contador=0;
       String dato= JOptionPane.showInputDialog("Dame el numero");
            cifras = Integer.parseInt(dato);
       
        while(cifras!=0){
           cifras = cifras/10;
           contador++;
       }
        
        System.out.println("El numero " + dato + " tiene " + contador + " cifras");
   }
   
   public static void matematicas(){       
    int resultado, dato, dato1;
    String cubo, parIm;
    String opcion=JOptionPane.showInputDialog("Introduce una opcion a calcular:\n1.- Cubo \n2.- Numero par o impar  \n3.- Surprise");
    switch(opcion)
    {
          case "1":
                   cubo =JOptionPane.showInputDialog("introduce el numero que deseas");
                   dato =Integer.parseInt(cubo);
                   resultado = dato*dato*dato;
                   System.out.println("El cubo del numero: " + cubo + " es: " + resultado);
                   break;
          case "2":
                   parIm =JOptionPane.showInputDialog("introduce el numero que deseas");
                   dato1=Integer.parseInt(parIm);
                   if(dato1 % 2 ==0 ){
                       System.out.println("El numero introducido: " + parIm + " es par");
                   }else{
                       System.out.println("El numero introducido: " + parIm + " es impar");
                   }
                   break;
          case "3":
                   break;
          default:
                   System.out.println("Haz lo que se te manda ceporro");
        }
    }
    //Ejemplo switch con Scanner
        public static void menuMatematico(){
            Scanner entrada = new Scanner(System.in);
            int opcion, numero;
            //double opcion2;
            System.out.println("Menu");
            System.out.println("[1] Cubo de un numero");
            System.out.println("[2] Numero par o impar");
            System.out.println("[3] Salir");
            System.out.print("Introduce una opcion(1-3): ");
            opcion = entrada.nextInt();
            //opcion2 = entrada.nextDouble();
            switch(opcion)
            {
                  case 1:
                              System.out.print("Introduce un número para obtener su cubo: ");
                              numero = entrada.nextInt();
                              int cubo = (int) Math.pow(numero,3);
                              System.out.println("El del numero es: " + cubo);
                              break;
                  case 2:
                              System.out.print("Introduce un número para saber si es par o impar: ");
                              numero = entrada.nextInt();
                              if(numero%2==0)
                                       System.out.println("El numero es par");
                              else
                                       System.out.println("El numero es impar");
                              break;
                  case 3:
                              break;
                  default:
                                       System.out.println("opcion no valida");
            }
        }
    
    public static void mayorEdad(){
        Scanner entrada = new Scanner(System.in);
        int años;
        System.out.print("Introduce tu edad: ");
        años = entrada.nextInt();
        
        if (años>=18){
            System.out.println("Eres mayor de edad");
        }else{
            System.out.println("Eres menor de edad");
        }  
    }
    
    public static void mayorEdadV1(){
        Scanner entrada = new Scanner(System.in);
        int años;
        System.out.print("Introduce tu edad: ");
        años = entrada.nextInt();
        
        while (años<18){
            System.out.println("Menor de edad");
            break;     
        }
        while (años>=18){
            System.out.println("Mayor de edad");
            break;     
        }
    }
    
    
    public static void promedio(){
        double nota1, nota2, nota3;
        String nombre;
        Scanner entrada = new Scanner(System.in);
        
        System.out.print("Pon el nombre del alumno: ");
        nombre = entrada.nextLine();
        System.out.print("Introduce la nota de la primera evaluacion: ");
        nota1 = entrada.nextDouble();
        System.out.print("Introduce la nota de la segunda evaluacion: ");
        nota2 = entrada.nextDouble();
        System.out.print("Introduce la nota de la tercera evaluacion: ");
        nota3 = entrada.nextDouble();
        System.out.println("El alumno " +nombre+" tiene una nota media de "+(nota1+nota2+nota3)/3);
    }
    
    /*Diseña un programa Java denominado PROG02_Ejerc8 que dados el número de alumnos
matriculados en Programación, número de alumnos matriculados en Entornos de Desarrollo y
número de alumnos matriculados en Base de datos. El programa deberá mostrar el % de alumnos
matriculado en cada uno de los tres módulos. Se supone que un alumno sólo puede estar
matrículado en un módulo. Trata de mostrar un solo decimal en los porcentajes.
*/
    public static void matriculados(){
        double progamacion, entorno, baseDatos, total;
        double porcentaje1, porcentaje2, porcentaje3;
        Scanner entrada = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("###.#");

        System.out.print("Introduce el numero de alumnos de Programacion: ");
        progamacion = entrada.nextDouble();
        
        System.out.print("Introduce el numero de alumnos de Entorno de Desarrollo: ");
        entorno = entrada.nextDouble();
        
        System.out.print("Introduce el numero de alumnos de Base de Datos: ");
        baseDatos = entrada.nextDouble();
        
        total = progamacion+entorno+baseDatos;
        
        porcentaje1= (progamacion*100)/total;
        System.out.println("Porcentaje de los de progamacion: "+ df.format(porcentaje1)+"%");
        
        porcentaje2= (entorno*100)/total;
        System.out.println("Porcentaje de los de progamacion: "+df.format(porcentaje2)+"%");
        
        porcentaje3= (baseDatos*100)/total;
        System.out.println("Porcentaje de los de progamacion: "+String.format("%.1f",porcentaje3)+"%");
    }
    
    /*Realiza un programa en Java al que le introduzcas 10 números por teclado. 
    Cuente el número de números positivos introducidos por teclado, 
    cuente el número de números negativos introducidos por teclado 
    y realice la suma de todos ellos.*/
    
    public static void cuentaNumeros(){
        int c = 0, numero = 0;
        int positivo=0;
        int negativo=0;
        int suma=0;
        Scanner entrada = new Scanner(System.in);
        for(c=0;c<=10;c++){
            System.out.print("Introduce el numero: ");
            numero = entrada.nextInt();
            if(numero<0){
                positivo++;
            }else{
                negativo++;
            }
            suma=suma+numero;
            System.out.println("Positivos "+ positivo);
            System.out.println("Negaticos "+ negativo);
            System.out.println("Todos: "+suma);
        }
    }
    
    /*Escribir un programa en Java que lea un número entero por el teclado 
        e imprima todos los números impares menores que él.*/
    
    
    public static void decremento(){
        int numero=0,c=0;
        Scanner entrada = new Scanner(System.in);
        System.out.print("Introduce el numero: ");
        numero = entrada.nextInt();
        for(c=numero;c>0;c--){
            if(c%2!=0){
                System.out.println(c);
            }
        }
    }
    
    /*Haz un programa que pida un numero de cuenta de veinte digitos 
    y muestre por pantalla la siguiente informacion:
    -Codigo del banco
    -Codigo de oficina
    -Digitos de control
    -Numero de cuenta
    Se debe comprobar que haya introducido el numero correcto de digitos
    */
    
    public static void banco(){
       String numero;
       do{
           numero = JOptionPane.showInputDialog("Introduce el numero");
       }while(numero.length()!=20);
       String codigoBancario=numero.substring(0,4);
       System.out.println(codigoBancario);
       String codigoOficina=numero.substring(4,8);
       System.out.println(codigoOficina);
       String digitoControl=numero.substring(8,10);
       System.out.println(digitoControl);
       String codNumCuen=numero.substring(10);
       System.out.println(codNumCuen);
       
       System.out.println("Tu numero de cuenta es " +numero);
       System.out.println("El codigo bancario es: " +codigoBancario);
       System.out.println("El codigo de la oficina es: " +codigoOficina);
       System.out.println("El digito de control es: " +digitoControl);
       System.out.println("El el numero de cuenta es: " +codNumCuen);
    }
    
    
    /*Pedir números al usuario y cuando el usuario meta un -1 se terminará el programa.
    Al terminar, mostrará lo siguiente:
        – mayor numero introducido
        – menor numero introducido
        – suma de todos los numeros
        – suma de los numeros positivos
        – suma de los numeros negativos
    El número -1 no contara para ningún cálculo, 
    únicamente para terminar el programa*/

    public static void muchoNumero(){
        int numero, menor=0, pos=0, nega=0, suma=0; 
        int mayor=0;
        String pide;        
        
        pide = JOptionPane.showInputDialog("Intorduce un numero (-1 para salir)");
        numero = Integer.parseInt(pide);
        
        while(numero!=-1){
            if(numero>=0)  pos+=numero; // Equivale a pos = pos + numero
            else  nega+=numero;
            if(numero>mayor) mayor=numero; 
            if(numero<menor) menor=numero;
            
            pide = JOptionPane.showInputDialog("Intorduce un numero (-1 para salir)");
            numero = Integer.parseInt(pide);
            
        }
            suma= pos + nega;
 
            System.out.println("El numero mayor es: " +mayor);
            System.out.println("El numero menor es: " +menor);
            System.out.println("La suma de los positivos es: " +pos);
            System.out.println("La suma de los negativos es: " +nega);
            System.out.println("La suma de los numero introducidos es: " +suma);
    }
    
    
    /*Pide un número por teclado e indica si es un número primo o no. Un número primo es aquel que 
      solo puede dividirse entre 1 y si mismo. 
      NOTA: Si se introduce un numero menor o igual que 1, directamente es no primo.*/
    
    public static void numPrimo(){
        boolean primo = true;
        int numero;
        Scanner entrada = new Scanner(System.in);
        System.out.print("Introduce el numero: ");
        numero = entrada.nextInt();
        int i = 2;
        // Con while
        while(i<numero && primo){
            if (numero % i == 0) primo = false;
            i++;
        }
       
        // Con for
        /*for(int c=2;c<numero;c++){
            // Si es dividible por otro número distinto de sí mismo y de 1, no es primo
            if(numero%c==0) primo = false;       
        }*/
        if (primo)System.out.println("El número ES primo");
        else System.out.println("El número NO es primo");
    }
    
    /*Lee un String por teclado y cuenta el número de vocales. 
        Utiliza para ello un un método de la clase String llamado charAt(i);*/
    
    public static void cuentaVocales(){
       String vocales = JOptionPane.showInputDialog("Pon la frase o palabra que quieras(sin acentos cacho carne): ");
       int contador = 0;
       //Scanner entrada = new Scanner(System.in);
       //System.out.print("Pon la frase o palabra que quieras(sin acentos cacho carne): ");
       //vocales = entrada.nextLine();
       vocales = vocales.toLowerCase();
       for (int c=0;c<vocales.length();c++){
           
            //ejemplo con if
           if (vocales.charAt(c)=='a' ||
               vocales.charAt(c)=='á' ||     
               vocales.charAt(c)=='e' ||
               vocales.charAt(c)=='é' ||    
               vocales.charAt(c)=='i' ||
               vocales.charAt(c)=='í' ||    
               vocales.charAt(c)=='o' ||
               vocales.charAt(c)=='ó' ||    
               vocales.charAt(c)=='u' ||
               vocales.charAt(c)=='ú'     
            )
           {
               contador++;
            }
           
           // Ejemplo con switch
          /* switch(vocales.charAt(c)){
               case 'a' : case 'á':
                   
               case 'e': case 'é' :
                   
               case 'i': case 'í' :
                   
               case 'o': case 'ó' :
                   
               case 'u': case 'ú' :
                   contador++;
                   break;
           }*/
        }
       System.out.println("La palabra " +vocales+ " tiene " +contador+ " vocales."); 
    }
    
        
}
      
   
