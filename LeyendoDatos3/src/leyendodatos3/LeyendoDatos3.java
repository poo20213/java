
package leyendodatos3;
import java.lang.Math;
import javax.swing.JOptionPane;
/**
 *
 * @author miguel
 */
public class LeyendoDatos3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String radio = JOptionPane.showInputDialog("Escribe el radio");  
        double n=Double.parseDouble(radio); 
        
        double x= Math.pow(n, 2);
        
        System.out.println(Math.PI*x);
    }
    
}
