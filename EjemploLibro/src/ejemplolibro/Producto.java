/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplolibro;

/**
 *
 * @author miguel
 */
public class Producto {
    int codigo;
    String descripcion;
    double precio;
// el constructor: inicializa el objeto Producto
Producto(int codigo, String descripcion, double precio) {
    this.codigo = codigo;
    this.descripcion = descripcion;
    this.precio = precio;
}
// otro producto: recibe solo el codigo
Producto(int codigo) {
    this.codigo = codigo;
    this.descripcion = "";
    this.precio = 0;
}
// fija el precio del producto setter
void fijarPrecio(double precioNuevo) {
    precio = precioNuevo;
}
// devuelve el precio del producto getter
double obtenerPrecio() {
    return precio;
    }
void fijarDescripcion(String descripcionNuevo){
    descripcion = descripcionNuevo;
}
String obtenerDescripcion(){
    return descripcion;
}
void fijarCodigo(int codigoNuevo){
    codigo = codigoNuevo;
}
int obtenerCodigo(){
    return codigo;
}

}
