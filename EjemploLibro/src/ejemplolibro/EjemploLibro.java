/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplolibro;

/**
 *
 * @author miguel
 */
public class EjemploLibro {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Producto sal, azúcar, leche;
        sal = new Producto(80005355, "Sal", 0.60);
        azúcar = new Producto(80005388, "Azúcar", 0.81);
        
        System.out.println("Precio de 1 paquete de sal: " +
        sal.obtenerPrecio() + " EUR");
        System.out.println("Precio de 1 paquete de azúcar: " +
        azúcar.obtenerPrecio() + " EUR");
        
        //Ampliacion del ejercicio con el producto leche
        leche = new Producto(801456550);
        leche.fijarDescripcion("leche entera el buen pastor");
        leche.fijarPrecio(0.9);
        System.out.println("decripcion de la leche: " + 
        leche.obtenerDescripcion() + " y precio: " + leche.obtenerPrecio());
    }
   
   
}
