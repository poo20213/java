
package cafeteraapp;

/**
 *
 * @author miguel
 */
public class Cafetera {
    private int capacidadMaxima;
    private int cantidadActual;
    
    // constructores

    public Cafetera() {
        this.capacidadMaxima = 1000;
        this.cantidadActual = 0;
    }
 
    public Cafetera(int capacidadMaxima) {
        this.capacidadMaxima = capacidadMaxima;
        this.cantidadActual = 0;
    }
    
    public Cafetera(int capacidadMaxima, int cantidadActual) {
        this.capacidadMaxima = capacidadMaxima;
        if (cantidadActual>capacidadMaxima) {
            this.cantidadActual = capacidadMaxima;
        }else{       
            this.cantidadActual = cantidadActual;
        }
    }


    // Setters y getters

    public int getCapacidadMaxima() {
        return capacidadMaxima;
    }

    public void setCapacidadMaxima(int capacidadMaxima) {
        this.capacidadMaxima = capacidadMaxima;
    }

    public int getCantidadActual() {
        return cantidadActual;
    }

    public void setCantidadActual(int cantidadActual) {
        this.cantidadActual = cantidadActual;
    }
    
    
    // Metodos
    
    public void llenarCafetera(){
        this.cantidadActual = this.capacidadMaxima;
    }
    
    public void servirTaza(int cantidad){
        if (this.cantidadActual<cantidad) {
            this.cantidadActual=0;
            System.out.println("Haz café coño");
        }else{
            this.cantidadActual = this.cantidadActual - cantidad;
        }
    }
    
    public void vaciarCafetera(){
        this.cantidadActual = 0;
    }
    
    public void agregarCafe(int cantidad){
        if (this.capacidadMaxima >= this.cantidadActual+cantidad) {
            this.cantidadActual = this.cantidadActual+cantidad;
        }else{
            this.cantidadActual= this.capacidadMaxima;
            System.out.println("Mira como has puesto todo SUSU "+ (this.cantidadActual+cantidad-this.capacidadMaxima)+ " c.c que has tirado barrelo ahora.");
        }  
    }

    @Override
    public String toString() {
        return "Esta cafetera tiene una capacidad maxima de: " + capacidadMaxima + "c.c, y el deposito esta lleno con " + cantidadActual + " c.c";
    }
    
    /*public String ocupacion(){
        return "La cafetera esta al:" + (this.cantidadActual*100)/this.capacidadMaxima+"% de capacidad.";
    }*/
    
    public double ocupacion(){
        return (double) this.cantidadActual / this.capacidadMaxima * 100;

    }

}
