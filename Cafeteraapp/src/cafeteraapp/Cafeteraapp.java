
package cafeteraapp;

/**
 *
 * @author miguel
 */
public class Cafeteraapp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Cafetera bialetti, moka, taurus;
        
        bialetti = new Cafetera();
        moka = new Cafetera(2000);
        taurus = new Cafetera(2000,750);
        
        bialetti.setCapacidadMaxima(3000);
        moka.vaciarCafetera();
        taurus.llenarCafetera();
        moka.servirTaza(100);
        moka.agregarCafe(1500);
        moka.agregarCafe(800);
        taurus.servirTaza(800);
        
        System.out.println(bialetti.toString());
        System.out.println(moka.toString());
        System.out.println(taurus.toString());
        
        System.out.println(bialetti.ocupacion());
        System.out.println(moka.ocupacion());
        System.out.println(taurus.ocupacion());
    }
    
}
