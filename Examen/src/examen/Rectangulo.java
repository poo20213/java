
package examen;

/**
 *
 * @author miguel
 */
public class Rectangulo {
    private String nombre;
    private double base;
    private double altura;

    public Rectangulo() {
        this.nombre="";
        this.base=0;
        
    }

    public Rectangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    public Rectangulo(String nombre, double base, double altura) {
        this.nombre = nombre;
        this.base = base;
        this.altura = altura;
    }
    
    
}
