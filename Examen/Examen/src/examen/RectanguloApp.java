
package examen;

/**
 *
 * @author miguel
 */
public class RectanguloApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Rectangulo rect1, rect2, rect3;
        
        rect1 = new Rectangulo();
        rect2 = new Rectangulo(4, 7);
        rect3 = new Rectangulo("Rectángulo3", 5, 5);
        
        
        // Rect1
        rect1.setNombre("Rectángulo1");
        rect1.setAltura(12);
        rect1.setBase(15);
        
        System.out.println(rect1.toString());
        
        System.out.println(rect1.isCuadrado());
        
        // pregunto si rect1.isCuadrado() es true para que devuelva una frase y no true o false
        if (rect1.isCuadrado()==true){
            System.out.println("Es cuadrado");
        }else{
            System.out.println("No es cuadrado");
        }
        
        // No lo pide pero imprimo los getter de rect3 para comprobar
        System.out.println(rect1.getAltura());
        System.out.println(rect1.getBase());
        System.out.println(rect1.getArea());
        System.out.println(rect1.getNombre());     
        
        // Salto de linea para que no quede junto
        System.out.println();
        
        // Rect2
        rect2.setNombre("Rectángulo2");
        System.out.println(rect2.toString());
        
        System.out.println(rect2.isCuadrado());
        
        // pregunto si rect2.isCuadrado() es true para que devuelva una frase y no true o false
        if (rect2.isCuadrado()==true){
            System.out.println("Es cuadrado");
        }else{
            System.out.println("No es cuadrado");
        }
        
        // No lo pide pero imprimo los getter de rect2 para comprobar
        System.out.println(rect2.getAltura());
        System.out.println(rect2.getBase());
        System.out.println(rect2.getArea());
        System.out.println(rect2.getNombre());     
        
        // Salto de linea para que no quede junto
        System.out.println();
        
        
        // Rect3
        // pregunto si rect3.isCuadrado() es true para que devuelva una frase y no true o false
        System.out.println(rect3.toString());
        
        System.out.println(rect3.isCuadrado());
        
        if (rect3.isCuadrado()==true){
            System.out.println("Es cuadrado");
        }else{
            System.out.println("No es cuadrado");
        }
        
        // No lo pide pero imprimo los getter de rect3 para comprobar
        System.out.println(rect3.getAltura());
        System.out.println(rect3.getBase());
        System.out.println(rect3.getArea());
        System.out.println(rect3.getNombre());     
    }
    
}
