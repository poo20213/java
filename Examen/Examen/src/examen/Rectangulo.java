
package examen;

/**
 *
 * @author miguel
 */
public class Rectangulo {
    // Atributos
    private String nombre;
    private double base;
    private double altura;

    // Constructores
    // Constructor por defecto con los datos inicializados a cero o vacio
    public Rectangulo() {
        this.nombre="";
        this.base=0;
        this.altura=0;        
    }
    
    // Segundo constructor que devuelve la base y la altura y el nombre inicializado a vacio
    public Rectangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
        this.nombre="";
    }
    
    // Tercer constructor que devuelve todos los datos
    public Rectangulo(String nombre, double base, double altura) {
        this.nombre = nombre;
        this.base = base;
        this.altura = altura;
    }
    
    
    // Getter personalizado para que devuelva el area
    public double getArea(){
        return this.base*this.altura;
    }
    
    // Metodos
    @Override
    public String toString() {
        return "El "+this.nombre+" tiene un area de "+this.getArea()+" y una altura de "+this.altura;
    }
    
    public boolean isCuadrado(){
        if(this.altura==this.base){
            return true;
        }else{
            return false;
        }
    }

    // Setters
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    // Getters
    public String getNombre() {
        return nombre;
    }

    public double getBase() {
        return base;
    }

    public double getAltura() {
        return altura;
    }
    
    
    
    
}
