
package dniapp;

/**
 *
 * @author miguel
 */
public class Dniapp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Definid tre NIFs, dos con el contructor por defecto y otro con el que
        //recibe un parametro. Utiliza el metodo leer() con uno de los creados por defecto
        // Por ultimo, muestra por pantalla con toString() el estado de los tres
        Dni pepe, shouko, sakura;
        
        pepe = new Dni();
        shouko = new Dni();
        sakura = new Dni(74658128);
        
        pepe.leer();
        shouko.setNumero(84562845);
        pepe.mostrar();
        shouko.mostrar();
        sakura.mostrar();
    }
    
}
