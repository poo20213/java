
package dniapp;

import javax.swing.JOptionPane;

public class Dni {
   private int numero;
   private char letra;

    public Dni() {
        this.numero = 0;
        this.letra = ' ';
    }

    public Dni(int numero) {
        this.numero = numero;
        this.calcularLetra();
    }

    public void setNumero(int numero) {
        this.numero = numero;
        this.calcularLetra();
    }
    
    public void leer(){
        String pideDni = JOptionPane.showInputDialog("Metele calo, un numero de DNI: ");
        this.numero = Integer.parseInt(pideDni);
        this.calcularLetra();
    }
    
    public void mostrar(){
        System.out.println(this.numero + "-" + this.letra);
    }


        
    
    private void calcularLetra(){
        int resto;
        
        resto = this.numero%23; 
        char [] letraNif ={
            'T','R','W','A','G','M','Y',
            'F','P','D','X','B','N','J',
            'Z','S','Q','V','H','L','C',
            'K','E'};
        
        this.letra = letraNif[resto];
    }
}
