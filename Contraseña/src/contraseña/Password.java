package contraseña;


public class Password {
    //atributos
    private String contraseña;
    private int longitud;
    private final static int LONG_DEF = 8;

    //constructores
    // crea uns contraseña al azar
    public Password() {
        this(LONG_DEF);
    }

    public Password(int longitud) {
        this.longitud = longitud;
        this.contraseña = generaPassword();
    }
    
    //Getter y setter
    
    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }
    
    // Otros metodos
    
    public boolean esFuerte() {
        int mayus=0, minus=0, nums=0;
        for (int i = 0; i < this.contraseña.length(); i++) {

            if ((this.contraseña.charAt(i)) >= 97 && this.contraseña.charAt(i) < 122) {
                minus++;
            } else if ((this.contraseña.charAt(i)) >= 65 && this.contraseña.charAt(i) <= 90) {
                mayus++;               
            } else {
                nums++;
                
            }
        }
        if (mayus >=2 && minus >= 1 && nums > 5) {
            return true;
        }else{
            return false;
        }
    }      

    // genera una contraseña al azar con la longitud que este definida
    public String generaPassword() {
        String contrasena="";
        int eleccion;
        char minus,mayus,num;

        for (int i = 0; i < this.longitud; i++) {
             //generamos un numero aleatorio, segun este elige si añadir una minuscila, mayuscula o numero
            eleccion=(int) (Math.floor(Math.random()*3+1));  
            if (eleccion==1){   
                minus=(char)   (int) (Math.floor(Math.random() * (123 - 97)+ 97));
                contrasena+=minus;
            }
            if (eleccion==2) {
                mayus=(char)   (int) (Math.floor(Math.random() * (91 - 65)+65));
                contrasena+=mayus;
            };
            
            if (eleccion==3) {
                num= (char)  (int) (Math.floor(Math.random() * (58-48)+48));
                contrasena+=num;
            };   
        }                            
        return contrasena;    
    }
}    