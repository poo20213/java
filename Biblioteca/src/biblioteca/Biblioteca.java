/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import javax.swing.JOptionPane;

/**
 *
 * @author miguel
 */
public class Biblioteca {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Libro colmena;
        
        colmena = new Libro("La colmena","Camilo José Cela",10,0);
      
        if(colmena.prestamo()){
            System.out.println("Libro prestado con exito");
        }else{
            System.out.println("No se puede prestar el libro. IMBECIL");
        }
        
        colmena.mostrarDatos();
       
    /*Haz un menú con dos opciones:
   1. Introducir nuevo libro
   2. Salir
   Si elige la primera opción se le pedirá todos los datos del libro y se 
   instanciará un nuevo objeto. Cuando elija la opción 2 saldrá del BUCLE
       */
    String introduce=JOptionPane.showInputDialog("Opciones para escoger:\n1.- Meter libro \n2.- Salir");;
    String nuevoTitulo, nuevoAutor, numEjem;
    Libro [] libros = new Libro[100];
    int cuentalibros=0;
    while(!introduce.equals("2")){
        
        nuevoTitulo=JOptionPane.showInputDialog("Introduce el titulo: ");
        nuevoAutor=JOptionPane.showInputDialog("Introduce el autor: ");      
        numEjem=JOptionPane.showInputDialog("Pon el numero de ejemplares: ");
        
        libros[cuentalibros] =new Libro(nuevoTitulo,nuevoAutor,Integer.parseInt(numEjem),0);
        cuentalibros++;
        introduce=JOptionPane.showInputDialog("Opciones para escoger:\n1.- Meter libro \n2.- Salir");
        
    }
        for (int i = 0; i < cuentalibros; i++) {
            libros[i].mostrarDatos();
        }
    }
    
}
