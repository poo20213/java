/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author miguel
 */

/*
Clase Libro
Crea una clase llamada Libro que guarde la información de cada uno de los libros de una biblioteca. 
La clase debe guardar el título del libro, autor, número de ejemplares del libro y número de ejemplares prestados. 
La clase contendrá los siguientes métodos:
Constructor por defecto.
Constructor con todos los parámetros.
•	Métodos Setters/getters

*/
public class Libro {
    private String titulo;
    private String autor;
    private int ejemplares;
    private int prestados;
    
    
    //Constructores
    public Libro() {
    }

    public Libro(String titulo, String autor, int ejemplares, int prestados) {
        this.titulo = titulo;
        this.autor = autor;
        this.ejemplares = ejemplares;
        this.prestados = prestados;
    }

    

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getEjemplares() {
        return ejemplares;
    }

    public void setEjemplares(int ejemplares) {
        this.ejemplares = ejemplares;
    }

    public int getPrestados() {
        return prestados;
    }

    public void setPrestados(int prestados) {
        this.prestados = prestados;
    }
    
/*
Método préstamo que incremente el atributo correspondiente cada vez que se realice un préstamo del libro.
No se podrán prestar libros de los que no queden ejemplares disponibles para prestar.
Devuelve true si se ha podido realizar la operación y false en caso contrario.
*/   

    public boolean prestamo(){
        if(this.ejemplares <= this.prestados){ // si el numero de ejemplares es menor o igula al de los prestados, devolvemos falso.
            return false;
        } else { // si no, se suman prestados y devolvemos true.
            this.prestados++;
            return true;    
        }    
    }
    
/*
Método devolución que decremente el atributo correspondiente cuando se produzca la devolución de un libro.
No se podrán devolver libros que no se hayan prestado.
Devuelve true si se ha podido realizar la operación y false en caso contrario.
*/
    public boolean devolucion(){
        if (this.prestados>0) {
            this.prestados--;
            return true;
        }else{
            return false;
        }
    }
    
    //Método mostrarDatos() que muestre toda la información del libro

    public void mostrarDatos(){
        System.out.println("El libro "+this.titulo+" escrito por "+this.autor+" cuenta con "+this.ejemplares+" ejemplares y tenemos prestados"+this.prestados+".");
    }
}
