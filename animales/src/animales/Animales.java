
package animales;


public class Animales {

    
    public static void main(String[] args) {
        
        //LO primero instanciamos los tres objetos de clase Animal
        Animal tigre, elefante, jirafa; 
        
        tigre = new Animal("Tigre(Felino)","carne",26,300,120);
        elefante = new Animal("Elefante(Paquidermo)","hierba", 90, 6350, 400);
        jirafa = new Animal("Jirafa","hojas",25,1000,600);
        
       //Animal tigre = new Animal("Tigre(Felino)","carne",80,250,4);
       //Animal elefante = new Animal("Elefante(Paquidermo)","hierba", 100, 4000, 8);
       //Animal jirafa = new Animal("Jirafa","hojas",50,300,10);
        
       // Declaro tres variables para almacenar las proporciones
       double proptigre, propelefante, projirafa;

        tigre.obtenLongyPeso();
        elefante.obtenEspyAli();
        jirafa.obtenEspyAli();
        
        proptigre = tigre.proporcion();
        propelefante = elefante.proporcion();
        projirafa = jirafa.proporcion();
        
        System.out.println("Las proporciones de tigre, elefante y jirafa son: " + proptigre + ", "+propelefante+" y " +projirafa);    
        
        tigre.obtenLong();
        elefante.obtenLong();
        jirafa.obtenLong();
    }  
    
    
}
