/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animales;

/**
 *
 * @author miguel
 */
public class Animal {
    private String especie;
    private String alimentacion;
    private int longevidad;
    private int maxpeso; // maxPeso
    private int maxaltura; // maxAltura
    
    //contructores
    public Animal(String especie, String alimentacion, int longevidad, int maxpeso, int maxaltura){
        this.especie = especie;
        this.alimentacion = alimentacion;
        this.longevidad = longevidad;
        this.maxpeso = maxpeso;
        this.maxaltura = maxaltura;
    }
    public Animal(String especie, String alimentacion){
        this.especie = especie;
        this.alimentacion = alimentacion;
        this.longevidad = 0;
        this.maxpeso = 0;
        this.maxaltura = 0;
    }
    
    //Metodos
    public void fijaEspyAli(String especie, String alimentacion){
        this.especie = especie;
        this.alimentacion = alimentacion;
    }
    
    public void fijaLongyPeso(int longevidad, int maxpeso){
        this.longevidad = longevidad;
        this.maxpeso = maxpeso;
    }
    
    public void obtenEspyAli(){
        System.out.println("El animal pertenece a la especie " + this.especie + " y se alimenta de  " + this.alimentacion);
    }
    
    public void obtenLongyPeso(){
        System.out.println("El animal puede vivir " + this.longevidad + " años y alcanzar un peso maximo de " + this.maxpeso);
    }
    
    public double proporcion(){
        double propor,a,p;
        a= this.maxaltura;
        p = this.maxpeso;
        propor = a/p;
        return propor;
        //return this.maxaltura / this.maxpeso;  
    }
    
    public void obtenLong(){
        if (this.longevidad>=40){
            System.out.println("Menudo vicho mas viejo");
        }else{
            System.out.println("Que cachorro");
        }
    }
}
