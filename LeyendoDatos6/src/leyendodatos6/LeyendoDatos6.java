package leyendodatos6;
import javax.swing.JOptionPane;
/**
 *
 * @author miguel
 */
public class LeyendoDatos6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) { 
       String numero = JOptionPane.showInputDialog("Introduce primer numero");
       double n=Double.parseDouble(numero);
       
       String numero2 = JOptionPane.showInputDialog("Introduce primer numero");
       double n1=Double.parseDouble(numero2);
       
       String operador = JOptionPane.showInputDialog("Operador");
       
       
       switch (operador){
           case "+" : System.out.println(n+n1);
                    break;
           case "-" : System.out.println(n-n1);
                    break;
           case "*" : System.out.println(n*n1);
                    break;
           case "/" : System.out.println(n/n1);
                    break;
           default: System.out.println("incorrecto");
                    break;
       }
    }
    
    //Ejercicio molón: 1) Introducir 2 números por teclado y luego un carácter indicando la operación a realizar (+,-,*,/) y devolver el resultado de la operación, utilizando un switch;


public static void operando(){   
        double x, y, resultado;
        String op1=JOptionPane.showInputDialog("Introduce un número");
        x=Double.parseDouble(op1);
        String op2=JOptionPane.showInputDialog("Introduce otro número");
        y=Double.parseDouble(op2);
        String operador=JOptionPane.showInputDialog("Introduce el operador a utilizar");
        switch(operador)
        {
              case "-":
                             resultado = x-y;
                             System.out.println("La resta es: " + resultado);
                             break;
              case "+":
                             resultado = x+y;
                             System.out.println("La suma es: " + resultado);
                             break;
              case "*":
                             resultado = x*y;
                             System.out.println("La multiplicación es: " + resultado);
                             break;
              case "/":
                             if(y!=0)
                             {
                                    resultado = x/y;
                                   System.out.println("La división es: " + resultado);
                             }
                             else
                                   System.out.println("no se puede dividir entre cero");
                             break;
              default:
                            System.out.println("Algún dato introducido es erróneo");
        }
               
     }   
    
}
