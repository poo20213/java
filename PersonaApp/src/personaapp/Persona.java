
package personaapp;

/**
 *
 * @author miguel
 */
public class Persona {
    private String nombre;
    private int edad;
    private String dni;

    public Persona() {
        this.nombre = "";
        this.edad = 0;
        this.dni = "";
    }

    public Persona(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
        this.dni = "";
    }

    public Persona(String nombre, int edad, String dni) {
        this.nombre = nombre;
        this.edad = edad;
        this.dni = dni;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public String getDni() {
        return dni;
    }
    
    public void mostrar(){
        System.out.println("La persona con el nombre de "+this.nombre+" y el DNI "+this.dni+" tiene "+this.edad+" años."); 
    }
    
    public boolean mayorDeEdad(){
        if(this.edad<18){
            return false;
        }else{
            return true;
        }
    }
}
