
package personaapp;

/**
 *
 * @author miguel
 */
public class PersonaApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Persona p1, p2, p3;
        
        p1 = new Persona();
        p2 = new Persona("Fede", 18);
        p3 = new Persona("Ana", 26, "75486258-J");
        
        p1.setNombre("Sakura");
        p1.setEdad(16);
        p1.setDni("68425789-T");
        
        p1.mostrar();
        if(p1.mayorDeEdad()==true){
            System.out.println("Es mayor de edad, pero te estaremos vigilando.");
        }else{
            System.out.println("Es menor de edad, llevaoslo chumachos.");
        }
        
        System.out.println();
        
        p2.mostrar();
        p2.setDni("78245369-K");
        if(p2.mayorDeEdad()==true){
            System.out.println("Es mayor de edad, pero te estaremos vigilando.");
        }else{
            System.out.println("Es menor de edad, llevaoslo chumachos.");
        } 
        
        System.out.println();
        
        System.out.println(p3.getNombre());
        System.out.println(p3.getEdad());
        System.out.println(p3.getDni());
        
        if(p3.mayorDeEdad()==true){
            System.out.println("Es mayor de edad, pero te estaremos vigilando.");
        }else{
            System.out.println("Es menor de edad, llevaoslo chumachos.");
        }        
        p3.mostrar();
    }
    
}
