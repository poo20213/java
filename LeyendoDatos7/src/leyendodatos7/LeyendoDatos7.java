package leyendodatos7;
import javax.swing.JOptionPane;
/**
 *
 * @author miguel
 */
public class LeyendoDatos7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        geometrica();
        //numeroDiasMes();
        //numeroMes();
    }    
    public static void numeroMes() { 
        int dias=0;
        String mes = JOptionPane.showInputDialog("Introduce el numero de mes");
        int numeroMes= Integer.parseInt(mes);
        
        switch(numeroMes){
            case 1: dias = 31;
                    break;
            case 2: dias = 28;
                    break;
            case 3: dias = 31;
                    break;
            case 4: dias = 30;
                    break;        
            case 5: dias = 30;
                    break;
            case 6: dias = 30;
                    break;
            case 7: dias = 31;
                    break;
            case 8: dias = 31;
                    break;        
            case 9: dias = 30;
                    break;
            case 10: dias = 31;
                    break;
            case 11: dias = 30;
                    break;
            case 12: dias = 31;
                    break;
            default: System.out.println("numero de mes equivocado");
        }
      System.out.println("El mes numero " + numeroMes +" tienes "+ dias +" dias");      
    }
    
    public static void numeroDiasMes() {
        int mes;
        double anno;
        String numero=JOptionPane.showInputDialog("Introduce un número de mes");
        mes=Integer.parseInt(numero);
        String numero2=JOptionPane.showInputDialog("Introduce un número de año");
        anno=Double.parseDouble(numero2);
        switch(mes)
            {
                  case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                               System.out.println("El mes tiene 31 dias");
                               break;
                  case 4: case 6: case 9: case 11:
                               System.out.println("El mes tiene 30 dias");
                               break;
                  case 2:
                               if((anno%4==0 && anno%100!=0) || anno%400==0)
                                       System.out.println("El mes tiene 29 dias");
                               else
                                       System.out.println("El mes tiene 28 dias");
                               break;
                  default:
                                       System.out.println("numero de mes equivocado");
            }
   }
    
    
    public static void geometrica(){
            String valor=JOptionPane.showInputDialog("Elija el numero de la figura\n1.-Circulo\n2.-Rectangulo\n3.-Triangulo\n4.-Cuadrado");
            int opcion=Integer.parseInt(valor);
  
            switch(opcion){
  
            case 1:{
                    valor=JOptionPane.showInputDialog("Radio");
                    double radio=Double.parseDouble(valor);
                    double area=Math.PI*(radio*radio);
                    System.out.println("El area del circulo es: "+area);
            }
                            break;
   
            case 2:{
                    valor=JOptionPane.showInputDialog("Base");
                    double base=Double.parseDouble(valor);
                    valor=JOptionPane.showInputDialog("Altura");
                    double altura=Double.parseDouble(valor);
                    double area=base*altura;
                    System.out.println("El area del rectangulo es: "+area);
            }
                            break;
   
            case 3:{
                    valor=JOptionPane.showInputDialog("Base");
                    double base=Double.parseDouble(valor);
                    valor=JOptionPane.showInputDialog("Altura");
                    double altura=Double.parseDouble(valor);
                    double area=(base*altura)/2;
                    System.out.println("El area del triangulo es: "+area);
            }
                            break;
            
            case 4: {
                    valor=JOptionPane.showInputDialog("Lado");
                    double lado=Double.parseDouble(valor);
                    double area = lado*lado;
                    System.out.println("El area del cuadrado es: "+area);
            }                
                            break;   
   default:
    System.out.println("Opcion no valida");
  }
 }
}

